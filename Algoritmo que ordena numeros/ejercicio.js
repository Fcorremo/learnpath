let unorderedItems = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 56, 123123, 23];

function orderItems(unorderedItems) {
  return unorderedItems.sort((a, b) => a - b);
}

console.log("INIT ARRAY:: " + unorderedItems);
console.log("FINAL ARRAY:: " + orderItems(unorderedItems));
