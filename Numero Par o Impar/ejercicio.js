const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('Ingrese un número: ', (numero) => {
  numero = parseInt(numero);

  if (numero % 2 === 0) {
    console.log(`${numero} es un número par.`);
  } else {
    console.log(`${numero} es un número impar.`);
  }

  rl.close();
});
