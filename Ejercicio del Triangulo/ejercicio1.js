const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('Ingrese un número: ', (numero) => {
  numero = parseInt(numero);

  if (numero > 0) {
    let triangulo = "";

    for (let i = 1; i <= numero; i++) {
      triangulo += "*".repeat(i);
      triangulo += "\n";
    }

    console.log(triangulo);
  } else {
    console.log("El número ingresado no es válido.");
  }

  rl.close();
});
